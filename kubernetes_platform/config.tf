## Set this to true when doing tests
## More information here: https://cloud.google.com/compute/docs/instances/preemptible 
variable "spotinst" { default = "true" }

## Define cluster perameters here
variable "name" { default = "verifa-demo" }
variable "description" { default = "verifa-demo-cluster" }
variable "project" { default = "verifa-terraform" }
## setting a "region" will create one node in each availability zone.
## eg. europe-north1 will create 3 master nodes, one in each zone while europe-north1-a will keep the cluster in one zone within that region.
variable "location" { default = "europe-north1-a" }

## Define DEFAULT node-pool parameters
variable "default_min_node" { default = 2 }
variable "default_max_node" { default = 10 }
variable "default_machine_type" { default = "n1-standard-2" }
## This sets yes/no to very cheap resources (only set to true while testing)
## More info: https://cloud.google.com/preemptible-vms/
variable "default_spotinst" { default = "true" }

## Define HIGHCPU node-pool parameters
variable "highcpu_min_node" { default = 1 }
variable "highcpu_max_node" { default = 20 }
variable "highcpu_machine_type" { default = "n1-highcpu-8" }
## This sets yes/no to very cheap resources (only set to true while testing)
## More info: https://cloud.google.com/preemptible-vms/
variable "highcpu_spotinst" { default = "true" }

## Define WORKER node-pool parameters
variable "worker_min_node" { default = 0 }
variable "worker_max_node" { default = 20 }
variable "worker_machine_type" { default = "n1-standard-2" }
## This sets yes/no to very cheap resources (only set to true while testing)
## More info: https://cloud.google.com/preemptible-vms/
variable "worker_spotinst" { default = "true" }

## Define node behavious
variable "repair" { default = "true" }
variable "upgrade" { default = "true" }
