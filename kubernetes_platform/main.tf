resource "google_container_cluster" "default" {
  name        = var.name
  project     = var.project
  description = var.description
  location    = var.location

  remove_default_node_pool = true
  initial_node_count       = var.default_min_node

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "default" {
  name       = "${var.name}-default-pool"
  project    = var.project
  location   = var.location
  cluster    = google_container_cluster.default.name
  node_count = var.default_min_node

  autoscaling {
    min_node_count = var.default_min_node
    max_node_count = var.default_max_node
  }

  management {
    auto_repair  = var.repair
    auto_upgrade = var.upgrade
  }

  node_config {
    preemptible  = var.spotinst
    machine_type = var.default_machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_container_node_pool" "highcpu" {
  name       = "${var.name}-highcpu-pool"
  project    = var.project
  location   = var.location
  cluster    = google_container_cluster.default.name
  node_count = var.highcpu_min_node

  autoscaling {
    min_node_count = var.highcpu_min_node
    max_node_count = var.highcpu_max_node
  }

  management {
    auto_repair  = var.repair
    auto_upgrade = var.upgrade
  }

  node_config {
    preemptible  = var.spotinst
    machine_type = var.highcpu_machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_container_node_pool" "worker" {
  name       = "${var.name}-worker-pool"
  project    = var.project
  location   = var.location
  cluster    = google_container_cluster.default.name
  node_count = var.worker_min_node

  autoscaling {
    min_node_count = var.worker_min_node
    max_node_count = var.worker_max_node
  }

  management {
    auto_repair  = var.repair
    auto_upgrade = var.upgrade
  }

  node_config {
    preemptible  = var.spotinst
    machine_type = var.worker_machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}